<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index()
    {
        $user_id = Auth::id();
        $profile = Profile::where('user_id', $user_id)->first();
        return view('profile.index', [
            'profile' => $profile
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => ['required', 'string', 'max:45'],
            'alamat' => ['required'],
            'umur' => ['required'],
        ]);

        $profile = Profile::find($id);

        $profile->nama = $request->nama;
        $profile->alamat = $request->alamat;
        $profile->umur = $request->umur;

        $profile->save();

        $request->session()->flash('success-update');
        return redirect('/profile');
    }
}
