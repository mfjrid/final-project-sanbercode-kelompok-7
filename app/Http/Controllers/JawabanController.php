<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Jawaban;
use illuminate\Support\Facades\Auth;

class JawabanController extends Controller
{
    public function tambah(Request $request, $id)
    {
        $request->validate([
            'jawaban' => 'required',
        ]);

        $jawaban = new Jawaban;

        $jawaban->user_id = Auth::id();
        $jawaban->jawaban = $request->jawaban;
        $jawaban->pertanyaan_id = $id;
        $jawaban->save();

        return redirect('/pertanyaan/' . $id);
    }

    public function destroy($pert_id, $id)
    {
        $jawaban = Jawaban::find($id);
        $jawaban->delete();

        return redirect('/pertanyaan/' . $pert_id);
    }
}
