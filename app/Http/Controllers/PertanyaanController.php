<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pertanyaan;
use App\Models\Kategori;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;

class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pertanyaan = Pertanyaan::where('user_id', Auth::id())->get();
        return view('pertanyaan.tampil', ['pertanyaan' => $pertanyaan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori_id = Kategori::all();
        return view('pertanyaan.tambah', [
            'kategori' => $kategori_id
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kategori_id' => 'required',
            'judul' => 'required',
            'pertanyaan' => 'required',
        ]);

        $pertanyaan = new Pertanyaan;
        $pertanyaan->kategori_id = $request->kategori_id;
        $pertanyaan->judul = $request->judul;
        $pertanyaan->pertanyaan = $request->pertanyaan;
        $pertanyaan->user_id = Auth::id();
        $pertanyaan->save();

        return redirect('/pertanyaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pertanyaan = Pertanyaan::where('id', $id)->first();
        $kategori = Kategori::where('id', $pertanyaan->kategori_id)->first();
        $user = Profile::where('user_id', $pertanyaan->user_id)->first();
        return view('pertanyaan.detail', [
            'pertanyaan' => $pertanyaan,
            'kategori' => $kategori,
            'user' => $user,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        $kategori = Kategori::all();
        return view('pertanyaan.edit', ['pertanyaan' => $pertanyaan, 'kategori' => $kategori]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'pertanyaan' => 'required',
        ]);

        $pertanyaan = Pertanyaan::find($id);
        $pertanyaan->kategori_id = $request->kategori_id;
        $pertanyaan->judul = $request->judul;
        $pertanyaan->pertanyaan = $request->pertanyaan;
        $pertanyaan->save();

        return redirect('/pertanyaan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        $pertanyaan->delete();

        return redirect('/pertanyaan');
    }
}
