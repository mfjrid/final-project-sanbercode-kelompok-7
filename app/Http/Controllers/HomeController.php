<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Pertanyaan;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $pertanyaan = Pertanyaan::select('*')->orderByDesc('created_at')->get();
        return view('home', [
            'pertanyaan' => $pertanyaan
        ]);
    }
}
