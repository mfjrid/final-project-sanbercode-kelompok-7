
# Final Project - Sanbercode Laravel Batch 38
## Kelompok 7
#### Anggota Kelompok :

- Muhammad Fajar
- Anisa
- Aurelly Joeandani

## Tema Project :

Website Forum Tanya Jawab

## ERD
![ERD](https://forum7.my.id/images/erd.png)

## 🔗 Link Live Project
[![link](https://img.shields.io/badge/Link-Website%20Forum%20Tanya%20Jawab-blue?style=for-the-badge)](https://forum7.my.id/)

## 🔗 Link Video Penjelasan
[![link](https://img.shields.io/badge/Link-Video%20Penjelasan-red?style=for-the-badge)](https://youtu.be/MzilCEQb9OA)

## 🔗 Link Template Yang Digunakan
[![link](https://img.shields.io/badge/Link-Template-green?style=for-the-badge)](https://preview.tabler.io/)