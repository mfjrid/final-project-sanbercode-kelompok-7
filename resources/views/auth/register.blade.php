@extends('layouts.html-tag')

@section('body')
    <div class="page page-center">
        <div class="container container-tight py-4" style="max-width: 50rem;">
            <div class="text-center mb-4">
                <a href="." class="navbar-brand navbar-brand-autodark">
                    <h1>Forum7.</h1>
                </a>
            </div>
            <form class="card card-md" method="POST" action="{{ route('register') }}" autocomplete="off">
                @csrf
                <div class="card-body">
                    <h2 class="card-title text-center mb-4">Buat Akun Baru</h2>

                    <div class="row mb-3">
                        <label for="nama" class="col-md-4 col-form-label text-md-end">{{ __('Nama') }}</label>
                        <div class="col-md-6">
                            <input id="nama" type="text" class="form-control @error('nama') is-invalid @enderror"
                                name="nama" value="{{ old('nama') }}" required autocomplete="nama" autofocus
                                placeholder="Nama Kamu">
                            @error('nama')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Alamat Email') }}</label>
                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                name="email" value="{{ old('email') }}" required autocomplete="email"
                                placeholder="mail@example.com">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>
                        <div class="col-md-6">
                            <input id="password" type="password"
                                class="form-control @error('password') is-invalid @enderror" name="password" required
                                autocomplete="new-password" placeholder="Password Kamu">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="password-confirm"
                            class="col-md-4 col-form-label text-md-end">{{ __('Ulangi Password') }}</label>
                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                                required autocomplete="new-password" placeholder="Ketik Ulang Password">
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="umur" class="col-md-4 col-form-label text-md-end">{{ __('Umur') }}</label>
                        <div class="col-md-6">
                            <input id="umur" type="number" class="form-control @error('umur') is-invalid @enderror"
                                name="umur" value="{{ old('umur') }}" required autocomplete="umur" autofocus
                                placeholder="0" min="0">
                            @error('umur')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="alamat" class="col-md-4 col-form-label text-md-end">{{ __('Alamat') }}</label>
                        <div class="col-md-6">
                            <input id="alamat" type="text" class="form-control @error('alamat') is-invalid @enderror"
                                name="alamat" value="{{ old('alamat') }}" required autocomplete="alamat" autofocus
                                placeholder="Jakarta, Indonesia">
                            @error('alamat')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-0 pt-3">
                        <div class="col-md-4 offset-md-4">
                            <button type="submit" class="btn btn-primary btn-block w-100">
                                {{ __('Buat Akun') }}
                            </button>
                        </div>
                    </div>
                </div>
            </form>

            <div class="text-center text-muted mt-3">
                Sudah memiliki akun? <a href="./login" tabindex="-1">Login</a>
            </div>
        </div>
    </div>
@endsection
