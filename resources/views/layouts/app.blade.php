@extends('layouts.html-tag')

@section('body')
    <div class="page">
        <!-- Navbar -->
        @include('partials.navbar')

        <div class="page-wrapper">
            <!-- Page header -->
            @yield('header')

            <!-- Page body -->
            <div class="page-body">
                <div class="container-xl">
                    <!-- Content here -->
                    @yield('content')
                </div>
            </div>

            @include('partials.footer')
        </div>
    </div>
@endsection
