<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Forum7</title>

    <!-- Styles -->
    <link href="{{ asset('dist/css/tabler.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dist/css/tabler-vendors.min.css') }}" rel="stylesheet">
    <style>
        @import url('https://rsms.me/inter/inter.css');

        :root {
            --tblr-font-sans-serif: Inter, -apple-system, BlinkMacSystemFont, San Francisco, Segoe UI, Roboto, Helvetica Neue, sans-serif;
        }
    </style>
    <link rel="stylesheet" href="{{ url('/') }}/dist/libs/sweetalert2/sweetalert2.min.css">

    <script src="{{ url('/') }}/dist/libs/jquery/jquery.min.js"></script>
    <script src="{{ url('/') }}/dist/libs/sweetalert2/sweetalert2.min.js"></script>
</head>

<body>

    @yield('body')

    <script src="{{ asset('dist/js/tabler.min.js') }}" defer></script>
</body>

</html>
