@extends('layouts.app')

@section('header')
    <div class="page-header d-print-none">
        <div class="container-xl">
            <div class="row g-2 align-items-center">
                <div class="col">
                    <h2 class="page-title">
                        Profile Saya
                    </h2>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="row row-cards">
                <div class="col-12">
                    <div class="card">
                        <div class="row g-0">
                            <div class="col-3 d-none d-md-block border-end">
                                <div class="card-body">
                                    <h4 class="subheader">Pengaturan</h4>
                                    <div class="list-group list-group-transparent">
                                        <a href="#"
                                            class="list-group-item list-group-item-action d-flex align-items-center active">Akun
                                            Saya</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col d-flex flex-column">
                                <form action="/profile/{{ $profile->id }}" method="POST">
                                    @csrf
                                    @method('put')
                                    <div class="card-body">
                                        <h2 class="mb-4">Akun Saya</h2>
                                        <h3 class="card-title">Detail Profil</h3>
                                        <div class="row g-3">
                                            <div class="col-md">
                                                <div class="form-label">Nama</div>
                                                <input id="nama" type="text"
                                                    class="form-control @error('nama') is-invalid @enderror" name="nama"
                                                    required value="{{ $profile->nama }}" autocomplete="off">
                                                @error('nama')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-md">
                                                <div class="form-label">Alamat</div>
                                                <input id="alamat" type="text"
                                                    class="form-control @error('alamat') is-invalid @enderror"
                                                    name="alamat" value="{{ $profile->alamat }}" required
                                                    autocomplete="off">
                                                @error('alamat')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-md">
                                                <div class="form-label">Umur</div>
                                                <input id="umur" type="number"
                                                    class="form-control @error('umur') is-invalid @enderror" name="umur"
                                                    value="{{ $profile->umur }}" required min="0" autocomplete="off">
                                                @error('umur')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <h3 class="card-title mt-4">Email</h3>
                                        <p class="card-subtitle">Email yang digunakan untuk login.
                                        </p>
                                        <div>
                                            <div class="row g-2">
                                                <div class="col-auto">
                                                    <input id="email" type="email" class="form-control w-auto"
                                                        name="email" value="{{ $profile->user->email }}" required
                                                        disabled>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer bg-transparent mt-auto">
                                        <div class="btn-list justify-content-end">
                                            <button type="submit" class="btn btn-primary">
                                                Simpan Profil
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (session()->has('success-update'))
        <script>
            Swal.fire({
                title: 'Berhasil!',
                text: 'Data profile berhasil di-update.',
                icon: 'success',
                confirmButtonText: 'Okay',
            })
            Swal.getConfirmButton().classList.add('btn', 'btn-primary')
            Swal.getConfirmButton().style.backgroundColor = '#206bc4'
            Swal.getConfirmButton().classList.remove('swal2-confirm', 'swal2-styled', 'swal2-default-outline')
        </script>
    @endif
@endsection
