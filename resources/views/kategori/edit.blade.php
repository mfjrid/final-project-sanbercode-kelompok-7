@extends('layouts.app')

@section('header')
    <div class="page-header d-print-none">
        <div class="container-xl">
            <div class="row g-2 align-items-center">
                <div class="col">
                    <h2 class="page-title">
                        Edit Kategori
                    </h2>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <form action="/kategori/{{ $kategori->id }}" method="POST">
        @csrf
        @method('put')
        <div class="row">
            <div class="col-lg-10">
                <div class="row row-cards">
                    <div class="col-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12 col-xl-12">
                                        <div class="mb-3">
                                            <label class="form-label">Jenis Kategori</label>
                                            <input type="text" class="form-control" value="{{ $kategori->nama }}"
                                                name="nama" placeholder="Programming">
                                        </div>
                                    </div>
                                    @error('nama')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <div class="card-footer text-end">
                                        <div class="d-flex">
                                            <button type="submit" class="btn btn-primary ms-auto">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
