@extends('layouts.app')

@section('header')
    <!-- include summernote css/js -->
    <link href="{{ asset('/dist/libs/summernote/summernote.min.css') }}" rel="stylesheet">
    <script src="{{ asset('/dist/libs/summernote/summernote.min.js') }}"></script>

    <div class="page-header d-print-none">
        <div class="container-xl">
            <div class="row g-2 align-items-center">
                <div class="col">
                    <h2 class="page-title">
                        Pertanyaan
                    </h2>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="card">
        <form action="/pertanyaan" method="POST">
            @csrf
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label class="form-label">Judul Pertanyaan</label>
                            <input type="text" class="form-control @error('judul') is-invalid @enderror"
                                placeholder="Judul Pertanyaan" name="judul" id="judul">
                            @error('judul')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label class="form-label">Kategori</label>
                            <select name="kategori_id" id="kategori_id"
                                class="form-control @error('kategori_id') is-invalid @enderror" id="">
                                @forelse ($kategori as $item)
                                    <option value="{{ $item->id }}">{{ $item->nama }}
                                    </option>
                                @empty
                                    {{-- <option value="0">tidak ada kategori</option> --}}
                                @endforelse
                            </select>
                            @error('kategori_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label class="form-label">Isi Pertanyaan</label>
                    <textarea id="pertanyaan" name="pertanyaan" class="@error('pertanyaan') is-invalid @enderror"></textarea>
                    @error('pertanyaan')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Post Pertanyaan</button>
            </div>
        </form>
    </div>

    <script>
        $(document).ready(function() {
            $('#pertanyaan').summernote({
                placeholder: 'Isikan detail pertanyaan disini',
                tabsize: 2,
                height: 200
            });
            $('.note-group-select-from-files').hide()
        });
    </script>
@endsection
