@extends('layouts.app')

@section('header')
    <div class="page-header d-print-none">
        <div class="container-xl">
            <div class="row g-2 align-items-center">
                <div class="col">
                    <h2 class="page-title">
                        List Pertanyaan
                    </h2>
                </div>
                <div class="col-12 col-md-auto ms-auto d-print-none">
                    <div class="btn-list">
                        <a href="{{ url('/') }}/pertanyaan/create" class="btn btn-primary d-none d-sm-inline-block">
                            <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24"
                                viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                <line x1="12" y1="5" x2="12" y2="19"></line>
                                <line x1="5" y1="12" x2="19" y2="12"></line>
                            </svg>
                            Tambah Pertanyaan
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="row row-cards">
                <div class="col-12">
                    <div class="card">
                        <div class="table-responsive">
                            <table class="table table-vcenter card-table">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Pertanyaan</th>
                                        <th scope="col">Action</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($pertanyaan as $key => $item)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{ $item->judul }}</td>
                                            <td>
                                                <form action="/pertanyaan/{{ $item->id }}" method="POST"
                                                    id="form-pertanyaan{{ $item->id }}">
                                                    <a href="/pertanyaan/{{ $item->id }}"
                                                        class="btn btn-info btn-sm">Detail</a>
                                                    <a href="/pertanyaan/{{ $item->id }}/edit"
                                                        class="btn btn-success btn-sm">Edit</a>
                                                    @method('delete')
                                                    @csrf
                                                    <input type="button" value="Delete" class="btn btn-danger btn-sm"
                                                        id="delete-pertanyaan{{ $item->id }}">
                                                </form>
                                            </td>
                                        </tr>

                                        <script>
                                            $(document).on('click', '[id^=delete-pertanyaan{{ $item->id }}]', function(e) {
                                                e.preventDefault();
                                                Swal.fire({
                                                    title: 'Apakah kamu yakin?',
                                                    text: "Data yang sudah dihapus tidak dapat dikembalikan!",
                                                    icon: 'warning',
                                                    showCancelButton: true,
                                                    confirmButtonColor: '#206bc4',
                                                    cancelButtonColor: '#d63939',
                                                    confirmButtonText: 'Ya, hapus saja!'
                                                }).then((result) => {
                                                    if (result.isConfirmed) {
                                                        confirmed = true;
                                                        $('#form-pertanyaan{{ $item->id }}').submit();
                                                    }
                                                })
                                                return false
                                            });
                                        </script>
                                    @empty
                                        <tr>
                                            <td>Tidak ada data pertanyaan</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
