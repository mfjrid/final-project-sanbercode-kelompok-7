@extends('layouts.app')

@section('header')
    <!-- include summernote css/js -->
    <link href="{{ asset('/dist/libs/summernote/summernote.min.css') }}" rel="stylesheet">
    <script src="{{ asset('/dist/libs/summernote/summernote.min.js') }}"></script>

    <div class="page-header d-print-none">
        <div class="container-xl">
            <div class="row g-2 align-items-center">
                <div class="col">
                    <h2 class="page-title">
                        {{ $pertanyaan->judul }}
                    </h2>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <p class="datagrid-title">Pertanyaan</p>
            {!! $pertanyaan->pertanyaan !!}
            <br>
            <div class="datagrid">
                <div class="datagrid-item">
                    <div class="datagrid-title">Diposting Pada</div>
                    <div class="datagrid-content">{{ $pertanyaan->created_at }}</div>
                </div>
                <div class="datagrid-item">
                    <div class="datagrid-title">Diupdate Pada</div>
                    <div class="datagrid-content">{{ $pertanyaan->created_at }}</div>
                </div>
                <div class="datagrid-item">
                    <div class="datagrid-title">Oleh</div>
                    <div class="datagrid-content">{{ $user->nama }}</div>
                </div>
                <div class="datagrid-item">
                    <div class="datagrid-title">Kategori</div>
                    <div class="datagrid-content">
                        <span class="status status-green">
                            {{ $kategori->nama }}
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-11">
            <h4><b>List Jawaban</b></h4>
        </div>
    </div>
    @forelse ($pertanyaan->jawaban as $item)
        <div class="row mt-3">
            <div class="col-md-1"></div>
            <div class="col-md-11">
                <div class="card">
                    <div class="card-status-start bg-primary"></div>
                    <div class="card-body">
                        <h3 class="card-title">{{ $item->profile->nama }}</h3>
                        <p class="text-muted">{!! $item->jawaban !!}</p>
                        @if (Auth::user()->id == $item->user_id)
                            <form action="/jawaban/{{ $pertanyaan->id }}/{{ $item->id }}/delete" method="POST"
                                id="form-jawaban{{ $item->id }}">
                                @method('delete')
                                @csrf
                                <input type="button" value="Hapus" class="btn btn-danger btn-sm"
                                    id="delete-jawaban{{ $item->id }}">
                            </form>
                        @endif
                    </div>

                    <script>
                        $(document).on('click', '[id^=delete-jawaban{{ $item->id }}]', function(e) {
                            e.preventDefault();
                            Swal.fire({
                                title: 'Apakah kamu yakin?',
                                text: "Data yang sudah dihapus tidak dapat dikembalikan!",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#206bc4',
                                cancelButtonColor: '#d63939',
                                confirmButtonText: 'Ya, hapus saja!'
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    confirmed = true;
                                    $('#form-jawaban{{ $item->id }}').submit();
                                }
                            })
                            return false
                        });
                    </script>
                </div>
            </div>
        </div>
    @empty
        <div class="card">
            <h4>Belum ada komentar</h4>
        </div>
    @endforelse

    <hr>
    <h3><b>Berikan Jawabanmu</b></h3>
    <div class="card">
        <div class="card-body">
            <form action="/jawaban/{{ $pertanyaan->id }}" method='post'>
                @csrf
                <textarea name="jawaban" id="jawaban" class="form-control @error('jawaban') is-invalid @enderror"></textarea>
                @error('jawaban')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <button type="submit" class="mt-3 btn btn-primary">Kirim Jawaban</button>
            </form>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#jawaban').summernote({
                placeholder: 'Isikan jawaban kamu disini',
                tabsize: 2,
                height: 200
            });
            $('.note-group-select-from-files').hide()
        });
    </script>
@endsection
