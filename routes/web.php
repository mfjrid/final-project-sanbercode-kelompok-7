<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\PertanyaanController;
use App\Http\Controllers\JawabanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::middleware(['auth'])->group(function () {
    // Isi route yang hanya bisa diakses setelah login
    Route::resource('/profile', ProfileController::class)->only('index', 'update');
    Route::resource('/kategori', KategoriController::class)->except('show');
    Route::resource('/pertanyaan', PertanyaanController::class);
    Route::post('/jawaban/{pertanyaan_id}', [JawabanController::class, 'tambah']);
    Route::delete('/jawaban/{pertanyaan_id}/{jawaban_id}/delete', [JawabanController::class, 'destroy']);
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
